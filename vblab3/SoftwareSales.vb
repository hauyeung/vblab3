﻿Public Class SoftwareSales

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        Dim a As Integer
        Dim b As Integer
        Dim c As Integer
        Dim asucceed As Integer
        Dim bsucceed As Integer
        Dim csucceed As Integer
        asucceed = Integer.TryParse(txtPackageA.Text, a)
        bsucceed = Integer.TryParse(txtPackageB.Text, b)
        csucceed = Integer.TryParse(txtPackageC.Text, c)
        If a < 0 Or asucceed = 0 Then
            MessageBox.Show("Please enter a number larger than 0")
        End If
        If b < 0 Or bsucceed = 0 Then
            MessageBox.Show("Please enter a number larger than 0")
        End If
        If c < 0 Or csucceed = 0 Then
            MessageBox.Show("Please enter a number larger than 0")
        End If

        Dim atotal As Double
        Dim btotal As Double
        Dim ctotal As Double
        Dim total As Double
        Dim calc As calculate = New calculate()
        Const aprice As Double = 99
        Const bprice As Double = 199
        Const cprice As Double = 299
        atotal = calc.calsubtotal(aprice, a)
        btotal = calc.calsubtotal(bprice, b)
        ctotal = calc.calsubtotal(cprice, c)
        total = atotal + btotal + ctotal
        txtTotal.Text = String.Format("Package A: ${0} " + vbCrLf + "Package B: ${1} " + vbCrLf + "Package C: ${2} " + vbCrLf + vbCrLf + " Grand Total: ${3}", Format(atotal, "0.00").ToString(), Format(btotal, "0.00").ToString(), Format(ctotal, "0.00").ToString(), Format(total, "0.00").ToString())
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtPackageA.Text = ""
        txtPackageB.Text = ""
        txtPackageC.Text = ""
        txtTotal.Text = ""
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
    End Sub
End Class
