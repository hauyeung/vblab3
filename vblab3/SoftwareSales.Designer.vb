﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SoftwareSales
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtPackageA = New System.Windows.Forms.TextBox()
        Me.txtPackageB = New System.Windows.Forms.TextBox()
        Me.txtPackageC = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.grpQuantitySold = New System.Windows.Forms.GroupBox()
        Me.txtTotal = New System.Windows.Forms.RichTextBox()
        Me.grpQuantitySold.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtPackageA
        '
        Me.txtPackageA.Location = New System.Drawing.Point(94, 19)
        Me.txtPackageA.Name = "txtPackageA"
        Me.txtPackageA.Size = New System.Drawing.Size(100, 20)
        Me.txtPackageA.TabIndex = 0
        '
        'txtPackageB
        '
        Me.txtPackageB.Location = New System.Drawing.Point(94, 48)
        Me.txtPackageB.Name = "txtPackageB"
        Me.txtPackageB.Size = New System.Drawing.Size(100, 20)
        Me.txtPackageB.TabIndex = 1
        '
        'txtPackageC
        '
        Me.txtPackageC.Location = New System.Drawing.Point(94, 76)
        Me.txtPackageC.Name = "txtPackageC"
        Me.txtPackageC.Size = New System.Drawing.Size(100, 20)
        Me.txtPackageC.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Package A"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Package B"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Package C"
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(40, 221)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(75, 23)
        Me.btnCalculate.TabIndex = 6
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(138, 221)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 23)
        Me.btnClear.TabIndex = 7
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(230, 221)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'grpQuantitySold
        '
        Me.grpQuantitySold.Controls.Add(Me.txtPackageA)
        Me.grpQuantitySold.Controls.Add(Me.txtPackageB)
        Me.grpQuantitySold.Controls.Add(Me.txtPackageC)
        Me.grpQuantitySold.Controls.Add(Me.Label1)
        Me.grpQuantitySold.Controls.Add(Me.Label2)
        Me.grpQuantitySold.Controls.Add(Me.Label3)
        Me.grpQuantitySold.Location = New System.Drawing.Point(23, 12)
        Me.grpQuantitySold.Name = "grpQuantitySold"
        Me.grpQuantitySold.Size = New System.Drawing.Size(300, 118)
        Me.grpQuantitySold.TabIndex = 10
        Me.grpQuantitySold.TabStop = False
        Me.grpQuantitySold.Text = "Quantity Sold"
        '
        'txtTotal
        '
        Me.txtTotal.Enabled = False
        Me.txtTotal.Location = New System.Drawing.Point(23, 137)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(300, 78)
        Me.txtTotal.TabIndex = 11
        Me.txtTotal.Text = ""
        '
        'SoftwareSales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(348, 264)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.grpQuantitySold)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnCalculate)
        Me.Name = "SoftwareSales"
        Me.Text = "Software Sales"
        Me.grpQuantitySold.ResumeLayout(False)
        Me.grpQuantitySold.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtPackageA As System.Windows.Forms.TextBox
    Friend WithEvents txtPackageB As System.Windows.Forms.TextBox
    Friend WithEvents txtPackageC As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents grpQuantitySold As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotal As System.Windows.Forms.RichTextBox

End Class
