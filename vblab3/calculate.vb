﻿Public Class calculate
    Function calsubtotal(ByVal price As Double, ByVal quantity As Integer) As Double
        Dim subtotal As Double
        If quantity >= 100 Then
            subtotal = price * quantity * 0.5
        ElseIf quantity >= 50 And quantity <= 99 Then
            subtotal = price * quantity * 0.6
        ElseIf quantity >= 20 And quantity <= 49 Then
            subtotal = price * quantity * 0.7
        ElseIf quantity >= 10 And quantity <= 19 Then
            subtotal = price * quantity * 0.8
        ElseIf quantity >= 0 And quantity <= 9 Then
            subtotal = price * quantity
        End If
        Return subtotal
    End Function
End Class
